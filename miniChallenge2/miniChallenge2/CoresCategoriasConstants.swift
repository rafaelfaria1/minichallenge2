//
//  CoresCategoriasConstants.swift
//  miniChallenge2
//
//  Created by Rafael Alexandre Faria1 on 6/19/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//


class CoresCategoriasConstants {
    static let comida = UIColor(colorLiteralRed: 93/255, green: 93/255, blue: 93/255, alpha: 0.5)
    static let arrumacao = UIColor(colorLiteralRed: 150/255, green: 150/255, blue: 150/255, alpha: 0.5)
    static let limpeza = UIColor(colorLiteralRed: 200/255, green: 50/255, blue: 100/255, alpha: 0.5)
    static let reparos = UIColor(colorLiteralRed: 20/255, green: 20/255, blue: 200/255, alpha: 0.5)
    static let fudeu = UIColor(colorLiteralRed: 20/255, green: 20/255, blue: 200/255, alpha: 0.5)

    static let comidaFont = UIColor(red: 182.0/255.0, green: 96.0/255.0, blue: 60.0/255.0, alpha: 1.0)
    static let arrumacaoFont = UIColor(red: 65.0/255.0, green: 121.0/255.0, blue: 75.0/255.0, alpha: 1.0)
    static let fudeuFont = UIColor(red: 42.0/255.0, green: 161.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    static let reparosFont = UIColor(red: 188.0/255.0, green: 188.0/255.0, blue: 188.0/255.0, alpha: 1.0)
    static let limpezaFont = UIColor(red: 182.0/255.0, green: 159.0/255.0, blue: 60.0/255.0, alpha: 1.0)
}
