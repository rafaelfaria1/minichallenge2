//
//  ComodosViewController.swift
//  miniChallenge2
//
//  Created by Rafael Alexandre Faria1 on 6/20/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit
import Parse

class ComodosViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.topItem!.title = "Cômodos"
    }
    
    @IBAction func chooseRoomFunc(sender: AnyObject) {
        switch (sender as! UIButton).tag{
            case 0:
                performSegueWithIdentifier("chooseRoom", sender: "NXN4hZxtNP")
                break
            case 1:
                performSegueWithIdentifier("chooseRoom", sender: "4ZZff3Mj3n")
                break
            case 2:
                performSegueWithIdentifier("chooseRoom", sender: "H3qWHd5AE8")
                break
            case 3:
                performSegueWithIdentifier("chooseRoom", sender: "2UBQj7ELyU")
                break
            default:
                break
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "chooseRoom" {
            let next = segue.destinationViewController as! FeedTableViewController
            next.room = (sender as! String)
            next.category = nil
        }
    }
}