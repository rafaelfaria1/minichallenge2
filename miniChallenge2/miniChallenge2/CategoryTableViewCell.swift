//
//  CategoryTableViewCell.swift
//  miniChallenge2
//
//  Created by Fernando Cani on 6/19/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet var imgCategoryImage: UIImageView!
    @IBOutlet var lblCategoryName:  UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}