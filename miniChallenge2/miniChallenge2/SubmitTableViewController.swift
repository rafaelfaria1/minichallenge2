//
//  SubmitTableViewController.swift
//  miniChallenge2
//
//  Created by Ronald Campanari on 6/20/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit
import MobileCoreServices


class SubmitTableViewController: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIPickerViewDelegate {
    
    var index = NSIndexPath()
    var lastCell = SubmitTableViewCell()
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.topItem!.title = "Adicionar"
    }
    
    struct ScreenSize {
        static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    }
    
    var cropRect = CGRectMake(0, 0, 0, 0)
    
    @IBOutlet weak var buttonSubmit: UIBarButtonItem!
    var strTitle: String! = ""
    var strCateg: String! = "Comida"
    var str: String! = ""
    var strRecebe: Bool! = false
    var addRow: Int! = 0
    var aux: Bool! = false
    var nextStep: Bool! = false
    var stepCount: Int! = 0
    var picker:UIImagePickerController?=UIImagePickerController()
    var submitArray: NSMutableArray = []
    var artigo = ParseArticleHandler()
    
    let categs = ["Comida", "Organização", "Reparos", "Limpeza", "F*deu"]
    let titulos = ["Apresentação", "Passos"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker!.delegate = self
        buttonSubmit.enabled = false
        self.tableView.backgroundColor = UIColor(colorLiteralRed: 93/255, green: 93/255, blue: 93/255, alpha: 0.5)
        self.tableView.rowHeight = 415
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titulos[section]
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 { return 1 }
        else { return 0 + addRow! }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        strRecebe = false
        print("Allow editing")
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("User is editing")
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        strRecebe = true
        print("Editing is Done")
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("Clear is pressed")
        return true
    }
    
    //Função para limitar o numero de caracteres em um TextField
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let lenght = (textField.text!.characters.count + string.characters.count)
        
        if lenght > 140 {
            return false
        }else {
            return true
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! SubmitTableViewCell
        lastCell = cell
        index = indexPath

        cell.pickerCateg.delegate = self
        cell.addSubview(cell.pickerCateg)
        
        cell.backgroundColor = UIColor(colorLiteralRed: 41/255, green: 41/255, blue: 41/255, alpha: 1)
        
        cell.textTitle.attributedPlaceholder = NSAttributedString(string:"Adicione um Título para a dica",
            attributes:[NSForegroundColorAttributeName: UIColor(colorLiteralRed: 127/255, green: 127/255, blue: 127/255, alpha: 1)])
        
        cell.textStep.attributedPlaceholder = NSAttributedString(string:"Escreva a descrição do passo",
            attributes:[NSForegroundColorAttributeName: UIColor(colorLiteralRed: 127/255, green: 127/255, blue: 127/255, alpha: 1)])
        
//        if DeviceType.IS_IPHONE_6P {
//            cell.buttonComplete.setImage(UIImage(named: "Concluir Guia@2x"), forState: UIControlState.Normal)
//        }
//        else if DeviceType.IS_IPAD{
//            cell.buttonComplete.setImage(UIImage(named: "Concluir Guia@3x"), forState: UIControlState.Normal)
//        }
        
        stepCount = indexPath.row + 1
        
        if strTitle!.isEmpty && addRow == 1{
            if let title = cell.textTitle.text { //Não mexer neste warning, esta funcionando assim!
                strTitle = cell.textTitle.text!
            }
        }
        else{ }
        
        
        if indexPath.section == 0 {
            cell.labelSteps.hidden = true
            cell.textStep.hidden = true
            cell.lineSteps.hidden = true
            cell.textTitle.hidden = false
            cell.lineTitle.hidden = false
            cell.labelPhoto.text = "Adicione uma imagem de Capa"
            cell.buttonComplete.enabled = false
            cell.pickerCateg.hidden = true
        }
        else{
            cell.labelSteps.hidden = false
            cell.textStep.hidden = false
            cell.lineSteps.hidden = false
            cell.textTitle.hidden = true
            cell.lineTitle.hidden = true
            cell.labelPhoto.text = "Adicione uma imagem para o passo"
            cell.buttonComplete.enabled = true
            cell.labelSteps.text = "Passo \(stepCount)"
            cell.pickerCateg.hidden = true
            
            if !cell.textStep.text!.isEmpty{
                submitArray.insertObject(cell.textStep.text!, atIndex: indexPath.row)
            }
            else { }
            
            if aux! {
                cell.pickerCateg.hidden = false
            }
        }

        cell.textStep.text = ""
        return cell
        
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int {
    return 1
    }
    
    func pickerView(pickerView: UIPickerView!, numberOfRowsInComponent component: Int) -> Int{
    return categs.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        strCateg = categs[row]
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categs[row]
    }
    
    //Função para alterar a cor dos componentes da pickerView
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch (row){
        case 0:
            let attributedString = NSAttributedString(string: categs[row], attributes: [NSForegroundColorAttributeName : UIColor(red: 182.0/255.0, green: 96.0/255.0, blue: 60.0/255.0, alpha: 1.0)])
            return attributedString
        case 1:
            let attributedString = NSAttributedString(string: categs[row], attributes: [NSForegroundColorAttributeName : UIColor(red: 65.0/255.0, green: 121.0/255.0, blue: 75.0/255.0, alpha: 1.0)])
            return attributedString
        case 2:
            let attributedString = NSAttributedString(string: categs[row], attributes: [NSForegroundColorAttributeName : UIColor(red: 188.0/255.0, green: 188.0/255.0, blue: 188.0/255.0, alpha: 1.0)])
            return attributedString
        case 3:
            let attributedString = NSAttributedString(string: categs[row], attributes: [NSForegroundColorAttributeName : UIColor(red: 182.0/255.0, green: 159.0/255.0, blue: 60.0/255.0, alpha: 1.0)])
            return attributedString
        case 4:
            let attributedString = NSAttributedString(string: categs[row], attributes: [NSForegroundColorAttributeName : UIColor(red: 42.0/255.0, green: 161.0/255.0, blue: 200.0/255.0, alpha: 1.0)])
            return attributedString
        default:
            let attributedString = NSAttributedString(string: categs[row], attributes: [NSForegroundColorAttributeName : UIColor.whiteColor()])
            return attributedString
        }
        //return attributedString
    }
    
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "OK", style:.Default, handler: nil)
        alertVC.addAction(okAction)
        presentViewController(alertVC, animated: true, completion: nil)
    }

    @IBAction func addCoverPhoto(sender: AnyObject) {
        
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            picker = UIImagePickerController()
            picker!.delegate = self
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            let types = [kUTTypeImage]
            var mediaType = [String()]
            for type : CFString in types{
                mediaType.append(type as String)
            }
            picker!.mediaTypes = mediaType
            picker!.allowsEditing = true
            self.presentViewController(picker!, animated: true, completion: nil)
        }
        else {
            NSLog("No Camera.")
        }
    }
    
    /*

        ---------------Esse é o metedo que traz a imagem
    */
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        var originalImage:UIImage?, editedImage:UIImage?, imageToSave:UIImage?
        let compResult:CFComparisonResult = CFStringCompare(mediaType as NSString!, kUTTypeImage, CFStringCompareFlags.CompareCaseInsensitive)
        if ( compResult == CFComparisonResult.CompareEqualTo ) {
            
            editedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            if ( editedImage != nil ) {
                imageToSave = editedImage
            } else {
                imageToSave = originalImage
            }
            imageToSave = resizeImage(imageToSave!, width: 845, height: 845)
            //
            //Usar a imageToSave como imagem já no formato esperado.
            //
            //
        }
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func resizeImage( image : UIImage, width : CGFloat, height : CGFloat) -> UIImage {
        var newSize = CGSizeMake(width, height)
        let widthRatio = CGFloat( newSize.width / image.size.width)
        let heightRatio = CGFloat( newSize.height / image.size.height)
        if widthRatio > heightRatio {
            newSize = CGSizeMake(image.size.width * heightRatio, image.size.height*heightRatio)
        } else {
            newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio)
        }
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func submit(sender: AnyObject) {
        submitArray.removeLastObject()
        // Parametro 0: passos, 1: room categ, 2: categ, Indice 3: Titulo
    }
    
    @IBAction func addStep(sender: AnyObject) {
            addRow!++;
            tableView.reloadData()
    }
    
    @IBAction func enableSubmit(sender: AnyObject) {
        submitArray.insertObject(lastCell.textStep.text!, atIndex: index.row)

        aux! = !aux
        if aux! {
            buttonSubmit.enabled = true
        } else {
            buttonSubmit.enabled = false
        }
        tableView.reloadData()
    }
    
} //end class