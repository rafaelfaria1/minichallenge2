//
//  miniChallenge2-Bridging-Header.h
//  miniChallenge2
//
//  Created by Ronald Campanari on 6/15/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

#ifndef miniChallenge2_Bridging_Header_h
#define miniChallenge2_Bridging_Header_h

#import <Parse/Parse.h>
#import <Bolts/Bolts.h>
#import "ImportClass.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Bolts/BFAppLinkResolving.h>
#endif /* miniChallenge2_Bridging_Header_h */
