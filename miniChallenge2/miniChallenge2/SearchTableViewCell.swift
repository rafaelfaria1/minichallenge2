//
//  SearchTableViewCell.swift
//  miniChallenge2
//
//  Created by Rafael Alexandre Faria1 on 6/22/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit
import ParseUI

class SearchTableViewCell: PFTableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var bkgImage: PFImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}