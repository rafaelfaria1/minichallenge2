//
//  ParseArticleHandler.swift
//  miniChallenge2
//
//  Created by Wellington Monteiro on 6/14/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import Parse

// Manages operations related with articles to and from the Parse database
class ParseArticleHandler: UIViewController {
    
    func postArticle() -> Void {
        
//        ParseArticleHandler.getAllAvailableRooms()
//        ParseArticleHandler.getCategories()
        
        var artigo:     String      = ""
        var splitText1: NSArray     = []
        var category:   String      = ""
        var room:       String      = ""
        var title:      String      = ""
        var steps:      [AnyObject] = [AnyObject]()
        
        if let url = NSBundle.mainBundle().URLForResource("postArticle", withExtension: "txt") {
            do {
                artigo      = try String(contentsOfURL: url, usedEncoding: nil)
                splitText1  = artigo.componentsSeparatedByString("\n")
                category    = splitText1[0] as! String
                room        = splitText1[1] as! String
                title       = splitText1[2] as! String
                for valor in 3...(splitText1.count-1) {
                    if(valor % 2 == 0) {
                        let bundle      = NSBundle.mainBundle()
                        let urlImage    = bundle.pathForResource((splitText1[valor] as! String), ofType: "png")
                        let image       = UIImage(contentsOfFile: urlImage!)
                        steps.append(image!)
                    } else {
                        let text = splitText1[valor] as! String
                        steps.append(text)
                    }
                }
            } catch let error {
                print("Error loading text: \(error)")
            }
            print("Category: \(category)")
            print("Room: \(room)")
            print("Title: \(title)")
            print("Category: \(steps)")
            print("All Rooms: \(ParseArticleHandler.getAllAvailableRooms())")
//            postArticle(steps, rooms: [room], categories: [category], title: title)
        }
    }
    
//    3F5UkQBWw7/Fudeu
//    i5NSIYeQZO/Reparos
//    WaJQQRW2yE/Comida
//    QMxcCjZ2IQ/Limpeza
//    StQp1bnatE/Arrumação
//    
//    NXN4hZxtNP/Churrasqueira
//    N8xmvuGN9l/Banheiro
//    H3qWHd5AE8/Sala
//    4ZZff3Mj3n/Quarto
//    2UBQj7ELyU/Cozinha
    
    
    
    //PLACEHOLDER METHODS - USED FOR TESTS ONLY!
    //Submits a new article to the Parse server: USED FOR TESTING PURPOSES ONLY
//    func postArticle() -> Void {
//        let bundle = NSBundle.mainBundle()
//        let urlTest = bundle.pathForResource("Cassini", ofType: "png")
//        let image = UIImage(contentsOfFile: urlTest!)
//        
//        //Storing 5 times a random image (simulating the photos for different steps) and the steps
//        var texts = self.createPlaceholderSteps()
//        
//        var steps : [AnyObject] = [AnyObject]()
//        
//        for i in 1...5 {
//            if(i % 2 == 0) {
//                steps.append(image!)
//            } else {
//                let text = texts.last
//                texts.removeLast()
//                steps.append(text!)
//            }
//        }
//        
//        postArticle(steps, rooms: ParseArticleHandler.getAllAvailableRooms().keys.array, categories: ParseArticleHandler.getAllAvailableCategories().keys.array, title: "Test Article")
//    }
    
    //TODO: Avoid SQL Injection
    private func createPlaceholderSteps() -> [String] {
        return [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eleifend, justo at venenatis posuere, risus ante varius eros, ac porta libero lorem in lorem. Nam id tempus risus. Integer in sodales augue, sit amet sollicitudin mi. In ut sem et tellus efficitur sodales. Aenean ultricies lacus eget molestie malesuada. Sed malesuada, nibh convallis pellentesque mattis, odio enim dictum mauris, consequat gravida nibh metus vitae tortor. Mauris volutpat justo ac odio bibendum, quis aliquam tellus dapibus. Aenean viverra sagittis diam, in consectetur mi egestas eget. Sed rutrum tellus quis urna fermentum tristique. Etiam congue ipsum ac sodales accumsan. Mauris dolor lorem, imperdiet non erat in, aliquam suscipit nibh.",
            "In est sapien, posuere sed velit ac, commodo dictum neque. Nulla non iaculis nisi. Quisque ullamcorper lobortis consectetur. Vivamus feugiat erat purus, quis ullamcorper metus cursus at. Aliquam at vulputate leo, nec tristique dolor. Cras maximus eleifend est nec consectetur. Praesent quis lectus viverra, tincidunt dui ac, interdum quam. Quisque malesuada sem in felis scelerisque posuere. Aenean aliquet sodales semper. Mauris ultrices magna lorem. Vestibulum luctus, tellus a varius auctor, purus ex faucibus lacus, at bibendum felis ipsum vitae metus. Sed nibh nibh, suscipit sed sapien at, viverra efficitur felis. Curabitur vel enim ac nulla euismod posuere. Sed egestas eros sit amet dui viverra, eget rhoncus diam vulputate. Sed sagittis leo sed eros placerat molestie",
            "Donec tristique maximus efficitur. Proin non accumsan sem. Cras eget efficitur elit. Morbi eget turpis bibendum, sodales ipsum non, fermentum ante. Sed pellentesque tincidunt felis, eu ultricies sapien maximus quis. Praesent sed vestibulum nibh. Etiam ornare lectus augue, in semper risus rhoncus sed. Donec ut dignissim lorem. Phasellus at posuere dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu scelerisque felis. Maecenas nec facilisis nisl. Nunc pellentesque dignissim molestie. Fusce imperdiet finibus cursus. Suspendisse sit amet lorem eu eros posuere posuere et tempus eros. ",
            "Curabitur ut lectus ac orci viverra blandit. Fusce bibendum libero id orci dictum dictum. Maecenas semper nec erat non vulputate. Proin laoreet cursus lacus at lacinia. Mauris ac nulla a ligula feugiat gravida id et nunc. Duis ac est maximus, ultrices tellus posuere, venenatis lorem. Sed at odio et lorem condimentum rhoncus. Aliquam sodales tellus turpis, at porta massa laoreet auctor. Morbi dapibus at leo at imperdiet. Aliquam pellentesque, arcu sit amet consectetur porttitor, sem massa venenatis nulla, in malesuada lacus diam tristique odio. Nunc volutpat odio mi, id iaculis orci tempus non. "
        ]
    }
    //END PLACEHOLDER METHODS
    
    
    //START PUBLIC METHODS
    
    func postArticle(allSteps: [AnyObject], rooms: [String], categories: [String], title: String) -> String? {
        //Creating a new article
        let newArticle = PFObject(className: "Articles")
        let relation = newArticle.relationForKey("images")
        
        newArticle.setObject(title, forKey: "title")
        
        //Declaring the descriptive step (strings) dictionary
        var steps : [String:String] = [:]
        
        //Check all the entries received.
        //If a given entry is a string, store it in the steps dictionary.
        //If the entry is an UIImage, store the image, rename it to the step # and link to the article
        for entry in 0...(allSteps.count - 1) {
            switch allSteps[entry] {
                case is String:
                    steps["\(entry+1)"] = allSteps[entry] as? String
                case is UIImage:
                    //Convert the UIImage into NSData and store it in the Parse remote database
                    let data = UIImagePNGRepresentation(allSteps[entry] as! UIImage)
                    let image = ParseArticleHandler.postImage(data!, order: entry+1)
                    
                    //If the store was successful, then link it to the article
                    if (image != nil) {
                        relation.addObject(image!)
                    }
                default:
                    print("Unknown step received at the entry \(entry+1)")
            }
        }
        
        //Assigning rooms and categories to the article
        setRoomRelations(newArticle, rooms: rooms)
        setCategoryRelations(newArticle, categories: categories)
        
        //Assigning the step texts to the article
        setArticleSteps(newArticle, steps: steps)
        
        //Returns true if the article was successfully stored in the database and false if not
        if newArticle.save() {
            notifyAdministrators(newArticle)
            return newArticle.objectId
        } else {
            return nil
        }
    }
    
    //Post a new image into the Parse database
    static func postImage(imageToStore: NSData, order: Int) -> PFObject? {
        let imageFile = PFFile(name: "\(order).png", data: imageToStore)
        
        let newImage = PFObject(className: "Images")
        newImage.setObject(imageFile, forKey: "image")
        
        if newImage.save() {
            return newImage
        } else {
            return nil
        }
    }
    
    //Retrieve all the available categories in a Dictionary - the key is the objectId and the value is the category name
    static func getAllAvailableCategories() -> [String: String] {
        var availableCategories = [String: String]()
        let categories = getCategories()!
        
        for category in categories {
            let categoryName = category["name"] as? String
            
            if(categoryName != nil) {
                availableCategories[category.objectId!] = categoryName!
            }
        }
        
        return availableCategories
    }
    
    //Retrieve all the available rooms in a Dictionary - the key is the objectId and the value is the room name
    static func getAllAvailableRooms() -> [String: String] {
        var availableRooms = [String: String]()
        
        for room in getRooms()! {
            let roomName = room["name"] as? String
            
            if(roomName != nil) {
                availableRooms[room.objectId!] = roomName!
            }
        }
        
        return availableRooms
    }
    
    //Retrieve all the articles for a given room ID, sorted by the update time, descending.
    static func getAllArticlesForRoom(roomId : String) -> PFQuery {
        let query = PFQuery(className: "Articles")
        query.whereKey("rooms", equalTo: roomId)
        query.orderByDescending("updatedAt")
        return query
    }
    
    //Retrieve all the articles for a given category ID, sorted by the update time, descending.
    static func getAllArticlesForCategory(categoryId : String) -> PFQuery {
        let query = PFQuery(className: "Articles")
        query.whereKey("categories", equalTo: categoryId)
        query.orderByDescending("updatedAt")
        return query
    }
    
    //Retrieve all the articles containing a given string, sorted by the update time, descending.
    static func getAllArticlesForString(stringSearch : String) -> PFQuery {
        let query = PFQuery(className: "Articles")
        query.whereKey("title", containsString: stringSearch)
        query.orderByDescending("updatedAt")
        return query
    }
    
    //Retrieve all the articles sorted by the update time, descending.
    static func getAllArticles() -> PFQuery {
        let query = PFQuery(className: "Articles")
        query.orderByDescending("updatedAt")
        return query
    }
    
    //END PUBLIC METHODS
    
    
    //START PRIVATE METHODS
    
    //Retrieve all the categories from Parse
    private static func getCategories() -> [PFObject]? {
        let query = PFQuery(className: "Categories")
        let categories = query.findObjects() as! [PFObject]?
        PFObject.pinAllInBackground(categories)
        return categories
    }
    
    //Retrieve all the rooms from Parse
    private static func getRooms() -> [PFObject]? {
        let query = PFQuery(className: "Rooms")
        query.fromLocalDatastore()
        let rooms = query.findObjects() as! [PFObject]?
        PFObject.pinAllInBackground(rooms)
        return rooms
    }
    
    //Retrieve a single category from Parse
    private func getCategories(categoryId : String) -> PFObject? {
        let query = PFQuery(className: "Categories")
        query.fromLocalDatastore()
        query.whereKey("objectId", containsString: categoryId)
        return (query.findObjects() as! [PFObject]?)?.first
    }
    
    //Retrieve a single room from Parse
    private func getRooms(roomId : String) -> PFObject? {
        let query = PFQuery(className: "Rooms")
        query.fromLocalDatastore()
        query.whereKey("objectId", containsString: roomId)
        return (query.findObjects() as! [PFObject]?)?.first
    }
    
    //Add the relationships between a new article and its categories
    private func setCategoryRelations(newArticle : PFObject, categories : [String]?) -> Void {
        if (categories != nil) {
            let relation = newArticle.relationForKey("categories")
            for category in categories! {
                let cat = getCategories(category)
                if (cat != nil) {
                    relation.addObject(cat as PFObject!)
                }
            }
        }
    }
    
    //Add the relationships between a new article and its rooms
    private func setRoomRelations(newArticle : PFObject, rooms: [String]?) -> Void {
        if (rooms != nil) {
            let relation = newArticle.relationForKey("rooms")
            for room in rooms! {
                let roomObj = getRooms(room)
                if (roomObj != nil) {
                    relation.addObject(roomObj as PFObject!)
                }
            }
        }
    }
    
    //Set the step texts to a given article
    private func setArticleSteps(newArticle: PFObject, steps: [String:String]) -> Void {
        newArticle.setObject(steps, forKey: "steps")
    }
    
    //Notify the administrators on new articles being posted
    private func notifyAdministrators(newArticle: PFObject) -> Void {
        let title = newArticle.valueForKey("title") as! String
        let articleContent = "Novo artigo enviado\r\nTítulo: " + title
        PFCloud.callFunction("sendMail", withParameters: ["toEmail":"wellingtonrmonteiro@gmail.com","fromEmail":"wellingtonrmonteiro@gmail.com","fromName":"Se Vira","text":articleContent,"subject":"Se Vira - Novo artigo enviado"])
    }
    
    //END PRIVATE METHODS
}
