//
//  CategoryTableViewController.swift
//  miniChallenge2
//
//  Created by Fernando Cani on 6/19/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit

class CategoryTableViewController: UITableViewController {

    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.topItem!.title = "Categorias"
    }
    
    var category:     NSMutableArray      = []
    var categoryCell: NSMutableArray      = []
    var categoryImages: NSMutableArray = ["categoryAlimentacao",
        "categoryOrganizacao",
        "categoryTecnologia",
        "categoryReparos",
        "categoryLimpeza"]
    let navigationBarSize:  CGFloat = 44.0
    let statusBarSize:      CGFloat = 22.0
    var cellHeight:         CGFloat = 100
    var screenSize:         CGRect!
    var screenWidth:        CGFloat!
    var screenHeight:       CGFloat!
    var itensCount:         Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itensCount = categoryImages.count
        self.populateCategory()
        self.populateCell()
    }
    
    func populateCategory() {
        category.addObject("ALIMENTAÇÃO")
        category.addObject("ARRUMAÇÃO")
        category.addObject("F*DEU")
        category.addObject("REPAROS")
        category.addObject("LIMPEZA")
    }

    func populateCell() {
        for valor in 0...(category.count-1) {
            let cell: CategoryTableViewCell = tableView.dequeueReusableCellWithIdentifier("categoryCell") as! CategoryTableViewCell
            cell.imgCategoryImage.image = UIImage(named: categoryImages[valor] as! String)
            cell.lblCategoryName.text = category[valor] as? String

            if      valor == 0  {cell.lblCategoryName.textColor = CoresCategoriasConstants.comidaFont}
            else if valor == 1  {cell.lblCategoryName.textColor = CoresCategoriasConstants.arrumacaoFont}
            else if valor == 2  {cell.lblCategoryName.textColor = CoresCategoriasConstants.fudeuFont}
            else if valor == 3  {cell.lblCategoryName.textColor = CoresCategoriasConstants.reparosFont}
            else                {cell.lblCategoryName.textColor = CoresCategoriasConstants.limpezaFont}
            //UIColor(colorLiteralRed: 93/255, green: 93/255, blue: 93/255, alpha: 0.5)
            
            //cell.frame.size.height = cellHeight
            
            categoryCell.addObject(cell)
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return ((tableView.frame.height - navigationBarSize) / CGFloat(itensCount)) * 0.77
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return ((tableView.frame.height - navigationBarSize) / CGFloat(itensCount)) * 0.05
    }
    
    /*override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerHeight = ((tableView.frame.height - navigationBarSize) / CGFloat(itensCount)) * 0.1
        
        let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: CGFloat(tableView.bounds.size.width), height: headerHeight))
        
        headerView.backgroundColor = UIColor(red: 29.0/255.0, green: 29.0/255.0, blue: 29.0/255.0, alpha: 1.0)
        return headerView;
    }*/
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return category.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            performSegueWithIdentifier("chooseCategory", sender: "WaJQQRW2yE")
            break
        case 1:
            performSegueWithIdentifier("chooseCategory", sender: "StQp1bnatE")
            break
        case 2:
            performSegueWithIdentifier("chooseCategory", sender: "3F5UkQBWw7")
            break
        case 3:
            performSegueWithIdentifier("chooseCategory", sender: "i5NSIYeQZO")
            break
        case 4:
            performSegueWithIdentifier("chooseCategory", sender: "QMxcCjZ2IQ")
            break
        default:
            break
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "chooseCategory" {
            let next = segue.destinationViewController as! FeedTableViewController
            next.room = nil
            next.category = (sender! as! String)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return categoryCell[indexPath.row + indexPath.section] as! UITableViewCell
    }
}