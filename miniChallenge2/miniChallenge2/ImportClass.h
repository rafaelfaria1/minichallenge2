//
//  ImportClass.h
//  miniChallenge2
//
//  Created by Ronald Campanari on 6/16/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/Bolts.h>

@interface ImportClass : NSObject
-(void)testFunctionWithParseObject:(PFObject *)obj;
@end
