//
//  CategoriaTableViewCell.swift
//  miniChallenge2
//
//  Created by Rafael Alexandre Faria1 on 6/19/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit
import ParseUI

class FeedTableViewCell: PFTableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bkgImage: PFImageView!
    @IBOutlet weak var categoryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}