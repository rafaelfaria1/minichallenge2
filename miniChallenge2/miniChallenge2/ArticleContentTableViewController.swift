//
//  ArticleContentTableViewController.swift
//  miniChallenge2
//
//  Created by Wellington Monteiro on 6/18/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import Bolts

class ArticleContentTableViewController: UITableViewController {
    var article : PFObject? = nil
    var categoryColor : UIImage? = nil
    private var steps : [Int : AnyObject] = [:]
    private var textCellHeights : [Int : Float] = [:]
    private var firstImageStepIndex : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if article != nil {
            getStepsFromArticle()
        }
        
        if categoryColor == nil {
            categoryColor = UIImage(named: "telaTutorialOrganizacao")
        }
        
        self.clearsSelectionOnViewWillAppear = false
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.topItem!.title = "Artigo"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //The number of steps plus the first cell with the article name
        return steps.count + 1
    }

    //Sets the height of a given row depending on the type of data being shown (e.g. photo or text)
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let step = steps[indexPath.row]
        
        if (step != nil) {
            switch step! {
            case is PFFile:
                return self.tableView.frame.width * 0.7
            case is String:
                let height = textCellHeights[indexPath.row]
                if (height != nil){
                    return CGFloat(height! + 50)
                }
                return 80.0
            case is Int:
                return self.tableView.frame.width * 0.187
            default:
                return 60.0
            }
        }else if indexPath.row == 0{
            //If it is null, it may be the first cell (title and image)
            let height = textCellHeights[indexPath.row]
            if (height != nil){
                return CGFloat(height! + 50)
            }
            return 80.0
        }
        
        return 60.0
    }
    
    //Sets the data of a given row
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! ArticleTableViewCell
        let step = steps[indexPath.row]
        
        //Clear cell data, if any
        cell.descriptionLabel.text = ""
        cell.stepLabel.text = ""
        cell.imgView.hidden = true
        cell.stepBackgroundColor.hidden = true
        
        if (step != nil) {
            switch step! {
                case is String:
                    cell.descriptionLabel.text = step as? String
                    cell.descriptionLabel.sizeToFit()
                    textCellHeights[indexPath.row] = Float(cell.descriptionLabel.frame.height * 0.9)
                case is PFFile:
                    cell.imgView.hidden = false
                    cell.imgView.file = step as? PFFile
                    cell.imgView.loadInBackground()
                case is Int:
                    let stepNumber = step as? Int
                    cell.stepLabel.text = "Passo \(stepNumber!)"
                    cell.stepLabel.sizeToFit()
                    cell.stepBackgroundColor.contentMode = UIViewContentMode.ScaleToFill
                    cell.stepBackgroundColor.image = self.categoryColor
                    cell.stepBackgroundColor.hidden = false
                default:
                    print("No contents at the row \(indexPath.row)")
            }
        }else{
            //If it is null, it may be the first cell (title and image)
            switch(indexPath.row) {
                case 0:
                    if (self.firstImageStepIndex != nil)
                    {
                        cell.imgView.hidden = false
                        cell.imgView.file = steps[firstImageStepIndex!] as? PFFile
                        cell.imgView.alpha = 0.4
                        cell.imgView.loadInBackground()
                    }
                    cell.stepLabel.text = article!.valueForKey("title") as? String
                    cell.stepLabel.sizeToFit()
                    textCellHeights[indexPath.row] = Float(cell.descriptionLabel.frame.height)
                default:
                    cell.descriptionLabel.text = ""
                    cell.stepLabel.text = ""
                    cell.imgView.hidden = true
                    cell.stepBackgroundColor.hidden = true
            }
        }

        return cell
    }
    
    //Retrieve all the steps (both the text and the photos) from an article
    private func getStepsFromArticle() -> Void {
        //Retrieve all the image steps
        var imagesRetrieved : [Int : PFFile] = [:]
        let imageSteps = self.article!.valueForKey("images") as? PFRelation
        if (imageSteps != nil) {
            let images = imageSteps?.query()?.findObjects()
            if (images != nil) {
                for imageObject in (images as! [PFObject]) {
                    if imageObject.objectForKey("image") != nil
                    {
                        let imagefile = imageObject["image"] as! PFFile
                        let fileName = imagefile.name.componentsSeparatedByString(".").first!
                        let stepNumber = Int(fileName.componentsSeparatedByString("-").last!)
                        
                        imagesRetrieved[stepNumber!] = imagefile
                    }
                }
            }
        }
        
        //Retrieve all the text steps
        var textsRetrieved : [Int : String] = [:]
        let textSteps = self.article!.valueForKey("steps") as? [String:String]
        if (textSteps != nil) {
            for text in textSteps! {
                let stepNumber = Int(text.0)
                textsRetrieved[stepNumber!] = text.1
            }
        }
        
        //Organize all the steps, including the cells displaying the step #
        var currentCellNumber = 1
        var stepCounter = 1
        for var i = 1; i <= textsRetrieved.count + imagesRetrieved.count; i++ {
            //If this step belongs to a text
            if(textsRetrieved[i] != nil){
                steps[currentCellNumber] = stepCounter
                steps[currentCellNumber+1] = textsRetrieved[i]
                currentCellNumber += 2
                stepCounter++
            }else{
               //Otherwise, the current step belongs to an image
                steps[currentCellNumber] = imagesRetrieved[i]
                firstImageStepIndex = currentCellNumber
                currentCellNumber++
            }
        }
    }
}
