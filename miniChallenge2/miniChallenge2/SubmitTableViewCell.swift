//
//  SubmitTableViewCell.swift
//  miniChallenge2
//
//  Created by Ronald Campanari on 6/20/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit

class SubmitTableViewCell: UITableViewCell, UIPickerViewDelegate {
    @IBOutlet weak var buttonPhoto: UIButton!
    @IBOutlet weak var textTitle: UITextField!
    @IBOutlet weak var textStep: UITextField!
    @IBOutlet weak var labelSteps: UILabel!
    @IBOutlet weak var buttonComplete: UIButton!
    @IBOutlet weak var labelComplete: UILabel!
    @IBOutlet weak var labelAddSteps: UILabel!
    @IBOutlet weak var labelPhoto: UILabel!
    @IBOutlet weak var lineTitle: UIImageView!
    @IBOutlet weak var lineSteps: UIImageView!

    @IBOutlet weak var pickerCateg: UIPickerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
