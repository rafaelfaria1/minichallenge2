//
//  CategoriasTableViewController.swift
//  miniChallenge2
//
//  Created by Rafael Alexandre Faria1 on 6/19/15.
//  Copyright © 2015 Fernando Cani. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class FeedTableViewController: PFQueryTableViewController {
    
    var category : String?
    var room : String?
    
    //Método da PFQUeryTableViewController para informar qual é a query que o server deve retornar para pesquisa, se vir a partir de uma categoria ou room ou apenas feed
    override func queryForTable() -> PFQuery {
        if category != nil{
            return ParseArticleHandler.getAllArticlesForCategory(category!)
        } else if room != nil {
            return ParseArticleHandler.getAllArticlesForRoom(room!)
        }
        return ParseArticleHandler.getAllArticles()
    }
    
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.parseClassName = "Articles"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = true
        self.objectsPerPage = 10
    }
    
    override func viewWillAppear(animated: Bool) {
        if category != nil {
            let titles = ParseArticleHandler.getAllAvailableRooms()
            self.navigationController!.navigationBar.topItem!.title = titles[category!]
        } else if room != nil {
            let titles = ParseArticleHandler.getAllAvailableRooms()
            self.navigationController!.navigationBar.topItem!.title = titles[room!]
        } else {
            self.navigationController!.navigationBar.topItem!.title = "Feed"
        }
//        let cor = UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)
//        self.navigationController?.navigationBar.barTintColor = cor
//        self.navigationController?.navigationBar.tintColor = UIColor(red: 37.0/255.0, green: 144.0/255.0, blue: 188.0/255.0, alpha: 1.0)
//        UITabBar.appearance().barTintColor = cor
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Teste Fernando
        ParseArticleHandler().postArticle()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.objects!.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell? {
        let objectCell = self.objects![indexPath.section] as? PFObject
        let cell = tableView.dequeueReusableCellWithIdentifier("feedCell", forIndexPath: indexPath) as! FeedTableViewCell
        cell.titleLbl.text = (objectCell!.objectForKey("title") as! String).localizedUppercaseString
        
        let imageSteps = objectCell?.valueForKey("images") as? PFRelation
        if (imageSteps != nil) {
            let images = imageSteps?.query()?.findObjects()
            if (images != nil) {
                let imageObject = images!.last as! PFObject
                if (imageObject.objectForKey("image") != nil){
                    cell.bkgImage.file = (imageObject.objectForKey("image") as! PFFile)
                    cell.bkgImage.loadInBackground()
        
                }
            }
        }
        
        var valueForKey : String = ""
        if category != nil {
            let categories = ParseArticleHandler.getAllAvailableCategories()
            if categories.count > 0 {
                valueForKey = categories[category!]!
            }
        } else {
            let categoriesRelation = objectCell?.objectForKey("categories") as? PFRelation
            if (categoriesRelation != nil){
                let categories = categoriesRelation!.query()!.findObjects()
                if (categories != nil){
                    if let categoryObject = categories?.first as? PFObject {
                        valueForKey = categoryObject.objectForKey("name") as! String
                    }
                }
            }
        }
        
        switch valueForKey {
        case "F*deu":
            cell.categoryImage.image = UIImage(named: "telaFeedTecnologia")
            break
        case "Reparos":
            cell.categoryImage.image = UIImage(named: "telaFeedReparos")
            break
        case "Comida":
            cell.categoryImage.image = UIImage(named: "telaFeedAlimentacao")
            break
        case "Limpeza":
            cell.categoryImage.image = UIImage(named: "telaFeedLimpeza")
            break
        case "Arrumação":
            cell.categoryImage.image = UIImage(named: "telaFeedOrganizacao")
            break
        default:
            cell.categoryImage.image = UIImage(named: "telaFeedTecnologia")
            break
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let object = self.objects![indexPath.section] as! PFObject
        let a = (object.objectForKey("title") as! String)
        print("\(a)")
        performSegueWithIdentifier("articles", sender: object)
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "articles"){
            let next = segue.destinationViewController as! ArticleContentTableViewController
            next.article = sender as? PFObject
            var valueForKey : String = ""
            if category != nil {
                let categories = ParseArticleHandler.getAllAvailableCategories()
                if categories.count > 0 {
                    valueForKey = categories[category!]!
                }
            } else {
                let categoriesRelation = (sender as! PFObject).objectForKey("categories") as? PFRelation
                if (categoriesRelation != nil){
                    let categories = categoriesRelation!.query()!.findObjects()
                    if (categories != nil){
                        if let categoryObject = categories?.first as? PFObject {
                             valueForKey = categoryObject.objectForKey("name") as! String
                        }
                    }
                }
            }
//            switch valueForKey {
//                case "F*deu":
//                    let image = UIImage(named: "tecnologiafeed")
//                    next.categoryColor = image
//                    break
//                case "Reparos":
//                    let image = UIImage(named: "reparosfeed")
//                    next.categoryColor = image
//                    break
//                case "Comida":
//                    let image = UIImage(named: "alimentacaofeed")
//                    next.categoryColor = image
//                    break
//                case "Limpeza":
//                    let image = UIImage(named: "limpezafeed")
//                    next.categoryColor = image
//                    break
//                case "Arrumação":
//                    let image = UIImage(named: "organizacaofeed")
//                    next.categoryColor = image
//                    break
//                default:
//                    let image = UIImage(named: "tecnologiafeed")
//                    next.categoryColor = image
//                            break
//            }
            switch valueForKey {
            case "F*deu":
                let image = UIImage(named: "tecnologiafeed")
                next.categoryColor = image
                break
            case "Reparos":
                let image = UIImage(named: "reparosfeed")
                next.categoryColor = image
                break
            case "Comida":
                let image = UIImage(named: "alimentacaofeed")
                next.categoryColor = image
                break
            case "Limpeza":
                let image = UIImage(named: "limpezafeed")
                next.categoryColor = image
                break
            case "Arrumação":
                let image = UIImage(named: "organizacaofeed")
                next.categoryColor = image
                break
            default:
                let image = UIImage(named: "tecnologiafeed")
                next.categoryColor = image
                break
            }
        }
    }
}